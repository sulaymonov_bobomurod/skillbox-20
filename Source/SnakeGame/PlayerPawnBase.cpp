// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	//CreateFoodActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!IsValid(FoodActor))
	{
		CreateFoodActor();
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlerPlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::CreateFoodActor()
{
	//FRotator StartingRotator = FRotator(0, 0, 0);

	float SpawnX = FMath::RandRange(MinX, MaxX);
	float SpawnY = FMath::RandRange(MinY, MaxY);

	FVector StartingLocation = FVector(SpawnX, SpawnY, SpawnZ);

	FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform(StartingLocation));
}

void APlayerPawnBase::HandlerPlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDerection != EMovementDerection::DOWN)
		{
			SnakeActor->LastMoveDerection = EMovementDerection::UP;
		}
		if (value < 0 && SnakeActor->LastMoveDerection != EMovementDerection::UP)
		{
			SnakeActor->LastMoveDerection = EMovementDerection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlerPlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDerection != EMovementDerection::LEFT)
		{
			SnakeActor->LastMoveDerection = EMovementDerection::RIGHT;
		}
		if (value < 0 && SnakeActor->LastMoveDerection != EMovementDerection::RIGHT)
		{
			SnakeActor->LastMoveDerection = EMovementDerection::LEFT;
		}
	}
}

